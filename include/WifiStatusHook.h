

#pragma once


class WifiStatusHook {
public:
    virtual void onStatusChange(int from, int to) {};

    virtual void wifiConnected() {};

    virtual void wifiDisconnected() {};
};



