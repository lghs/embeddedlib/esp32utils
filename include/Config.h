#ifndef CONFIG_H
#define CONFIG_H

#include <ArduinoJson.h>

/**
 * read spiffs file config.json
 */
class Config {

private:
    DynamicJsonDocument jsonDocument;

public:
    Config();

    void setup();
    /**
     * get a config value for a key
     */
    const char *get(char *key);


};


#endif //CONFIG_H
