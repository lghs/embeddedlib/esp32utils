

#pragma once

#include <WiFi.h>
#include "WifiStatusHook.h"

#define WIFI_TIME_BETWEEN_RECONNECT 5000

class WifiConnector {
    int lastStatus = 0;
    unsigned long lastReconnection = 0;

    void reconnect();

    WifiStatusHook *wifiStatusHook = 0;

public:
    WifiConnector() {};

    void setup(const char *ssid, const char *password, const char *hostname, bool isAp);

    void run();

    bool isConnected();

    void setWifiStatusHook(WifiStatusHook *hook);

};



