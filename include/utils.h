#pragma once


template< class T>
typename std::string to_string(T t)
{
    return string(String(t).c_str());
}
