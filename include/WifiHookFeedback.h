

#pragma once


#include <WifiStatusHook.h>
#include <VirtualLed.h>
#include <LedUtils.h>
#include <pattern/BlinkLedColorPattern.h>
#include "telnet/Telnet.h"

static BlinkLedColorPattern defaultWifiDisconnectedPattern(RED, FAST);
static SolidLedColorPattern defaultWifiConnectedPattern(GREEN);

class WifiHookFeedback : public WifiStatusHook{
    VirtualLed *indicator;
    bool setupTelnet;

public:
    WifiHookFeedback(VirtualLed *indicator, bool setupTelnet = false);
    void wifiConnected() override;
    void wifiDisconnected() override;

    LedColorPattern *wifiDisconnectedPattern = &defaultWifiDisconnectedPattern;
    LedColorPattern *wifiConnectedPattern = &defaultWifiConnectedPattern;
};



