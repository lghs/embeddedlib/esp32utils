#include "ota/Ota.h"


#define OTA_LOG_LEVEL _IMPORTANT
#include <ArduinoOTA.h>
#include "Log.h"
void Ota::setup() {
    ArduinoOTA.onStart([]() {
                String type;
                if (ArduinoOTA.getCommand() == U_FLASH)
                    type = "sketch";
                else // U_SPIFFS
                    type = "filesystem";

                // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
                DEBUGPRINTF(OTA_LOG_LEVEL,"Start updating %s\n", type);
            })
            .onEnd([]() {
                DEBUGPRINTF(OTA_LOG_LEVEL,"ota End\n");
            })
            .onProgress([](unsigned int progress, unsigned int total) {
                DEBUGPRINTF(OTA_LOG_LEVEL,"Progress: %u%%\r\n", (progress / (total / 100)));
            })
            .onError([](ota_error_t error) {
                DEBUGPRINTF(OTA_LOG_LEVEL, "Error[%u]: ", error);
                if (error == OTA_AUTH_ERROR) DEBUGPRINTF(_TRACE,"Auth Failed");
                else if (error == OTA_BEGIN_ERROR) DEBUGPRINTF(_TRACE,"Begin Failed");
                else if (error == OTA_CONNECT_ERROR) DEBUGPRINTF(_TRACE,"Connect Failed");
                else if (error == OTA_RECEIVE_ERROR) DEBUGPRINTF(_TRACE,"Receive Failed");
                else if (error == OTA_END_ERROR) DEBUGPRINTF(_TRACE,"End Failed");
            });

    ArduinoOTA.begin();
}

void Ota::run() {
    ArduinoOTA.handle();
}
