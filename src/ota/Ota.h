#pragma once
#include <Service.h>

class Ota : public Service{
public:
    void setup() override;
    void run() override;
};