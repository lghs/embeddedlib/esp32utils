

#include "WifiHookFeedback.h"

WifiHookFeedback::WifiHookFeedback(VirtualLed *indicator, bool setupTelnet) : indicator(indicator),
                                                                              setupTelnet(setupTelnet) {
}

void WifiHookFeedback::wifiConnected() {
    indicator->setPattern(wifiConnectedPattern);
    if(setupTelnet){
        Telnet::setup();
    }
}

void WifiHookFeedback::wifiDisconnected() {
    indicator->setPattern(wifiDisconnectedPattern);
}
