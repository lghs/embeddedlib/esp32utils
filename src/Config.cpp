#include <SPIFFS.h>
#include "Config.h"

Config::Config()
        : jsonDocument(2048) {
}

void Config::setup() {
    File config_file = SPIFFS.open("/config.json", "r");

    if (!config_file) {
        throw std::runtime_error("missing config file /config.json");
    }
    //Read the content
    String config_string = config_file.readString();
    deserializeJson(jsonDocument, config_string);
    config_file.close();
}

const char *Config::get(char *key) {
    if(!jsonDocument.containsKey(key)) {
        Serial.println(String("Configuration key missing : ") + String(key));
        throw std::runtime_error("configuration key missing ");
    }
    return jsonDocument[key];
}
