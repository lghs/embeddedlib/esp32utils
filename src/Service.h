#pragma once

class Service {
public:

    virtual void setup();

    virtual void run();
};
