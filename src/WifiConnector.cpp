

#include "WifiConnector.h"

void WifiConnector::reconnect() {
    unsigned long currentTime = millis();
    if (WiFi.status() != WL_CONNECTED
        && (currentTime - lastReconnection) > WIFI_TIME_BETWEEN_RECONNECT) {
        WiFi.reconnect();
        lastReconnection = currentTime;
    }
}

void WifiConnector::setup(const char *ssid, const char *password, const char *hostname, bool isAp = false) {

    Serial.print("Connecting to : ");
    Serial.println(ssid);

    if (isAp) {

        Serial.println("wifi ap");
        const IPAddress apIP = IPAddress(192, 168, 1, 1);
        WiFi.mode(WIFI_AP);
        WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
        WiFi.softAP(ssid, password);
    } else {
        WiFi.mode(WIFI_STA);
        Serial.println("wifi sta");
        WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE, INADDR_NONE);
        WiFi.setHostname(hostname);
        WiFi.begin(ssid, password);
    }

}

void WifiConnector::run() {

    wl_status_t currentStatus = WiFi.status();

    if (currentStatus != lastStatus) {
        Serial.print("Wifi status change from ");
        Serial.print(lastStatus);
        Serial.print(" to ");
        Serial.println(currentStatus);
        if (wifiStatusHook) {
            wifiStatusHook->onStatusChange(lastStatus, currentStatus);
        }

        if (currentStatus == WL_CONNECTED) {
            Serial.print("WiFi connected : ");
            Serial.print("IP address : ");
            Serial.println(WiFi.localIP());
            if (wifiStatusHook) {
                wifiStatusHook->wifiConnected();
            }
        } else {
            if (wifiStatusHook) {
                wifiStatusHook->wifiDisconnected();
            }
            reconnect();
        }
        lastStatus = currentStatus;
    } else if (currentStatus != WL_CONNECTED) {
        reconnect();
    }

}

bool WifiConnector::isConnected() {
    //use last status instead of wifi.status to be sure the connected hook has been triggered
    return lastStatus == WL_CONNECTED;
}

void WifiConnector::setWifiStatusHook(WifiStatusHook *hook) {
    this->wifiStatusHook = hook;
}