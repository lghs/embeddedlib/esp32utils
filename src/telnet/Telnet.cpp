#include "Telnet.h"


void (*Telnet::processInput)(String message) = 0;

void Telnet::setup() {
    telnet_sessionstate = NOCONNECTION;
    telnet.onConnect(Telnet::onTelnetConnect);
    telnet.onConnectionAttempt(onTelnetConnectionAttempt);
    telnet.onReconnect(onTelnetReconnect);
    telnet.onDisconnect(onTelnetDisconnect);
    telnet.onInputReceived(onTelnetInputReceived);

    Serial.print("- Telnet: ");
    if (telnet.begin(TELNET_PORT)) {
        Serial.println("running");
    } else {
        Serial.println("error.");
    }
}

void Telnet::run() {
    telnet.loop();
}

void Telnet::onTelnetConnect(String ip) {
    Serial.printf("- Telnet client %s connected\n", ip);
    Serial.printf("Telnet client %s  \n", ip);
    telnet_sessionstate = CONNECTED;
    Serial.printf( "Hello %s, welcome to %s\n", telnet.getIP(), welcome_message.c_str());
    Serial.printf( "bye to disconnect.\n");
}

void Telnet::onTelnetDisconnect(String ip) {
    Serial.printf( "- Telnet client %s disconnected\n", ip.c_str());
    Telnet::telnet_sessionstate = DISCONNECTED;
}

void Telnet::onTelnetReconnect(String ip) {
    Serial.printf( "- Telnet client %s reconnected\n", ip.c_str());
    Telnet::telnet_sessionstate = CONNECTED;
}

void Telnet::onTelnetConnectionAttempt(String ip) {
    Serial.printf( "- Telnet client %s tried to connect\n", ip.c_str());
    telnet_sessionstate = DISCONNECTED;
}

void Telnet::onTelnetInputReceived(String str) {
    Serial.print(str);
    if (str == "bye") {
        Serial.printf( "> disconnecting you...");
        telnet.disconnectClient();
    } else {
        if (processInput != 0) {
            processInput(str);
        }
    }
    //else do_gcode(str);
}

/* ------------------------------------------------- */

void Telnet::print(char* data) {
    if (telnet_sessionstate = CONNECTED) telnet.print(data);
}

int Telnet::telnet_sessionstate = 0;
ESPTelnet Telnet::telnet;
String Telnet::welcome_message = "";
