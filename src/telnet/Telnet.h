#pragma once

#include "ESPTelnet.h"

#define CONNECTED 1
#define NOCONNECTION 0
#define DISCONNECTED 2
#define TELNET_PORT 23

class Telnet {
public:
    static void setup();

    static void run();

    static void (*processInput)(String message);

    static void onTelnetConnect(String ip);

    static void onTelnetDisconnect(String ip);

    static void onTelnetReconnect(String ip);

    static void onTelnetConnectionAttempt(String ip);

    static void onTelnetInputReceived(String str);

    static void print(char* data);

    static ESPTelnet telnet;
    static int telnet_sessionstate;
    static String welcome_message;
};
