#pragma once
#include "Arduino.h"
#include "telnet/Telnet.h"

#define _TRACE 1
#define _INFO  2
#define _IMPORTANT  2
#define _ERROR 4
#define _CRITICAL 5
#define _DEBUG 5
#define DEBUG_VERBOSE 2
#define DEBUG_NONE 255

#define DEBUGLEVEL  _CRITICAL //DEBUG_VERBOSE
#define DEBUGPRINTF(level, ...) Log::printf(level, __VA_ARGS__)



class Log {
public:
    void static printf_direct(int level,const char* format, va_list args){
        if(level >= DEBUGLEVEL) {
            char buffer[256];
            sprintf(buffer,"%d:          ",millis());
            vsnprintf(buffer+10, 245, format, args);
            Serial.printf(buffer);
            Telnet::print(buffer);
        }
    }
    void static printf(int level,const char* format, ...){
        va_list args;
        va_start(args, format);
        printf_direct(level, format, args);
        va_end(args);
    }

    void static println(int level, String format, ...){
        va_list args;
        va_start(args, format);
        printf_direct(level, (format+"\n").c_str(), args);
        va_end(args);
    }

    void static debugln(String format, ...){
        va_list args;
        va_start(args, format);
        printf_direct(_DEBUG, (format + "\n").c_str(), args);
        va_end(args);
    }
};